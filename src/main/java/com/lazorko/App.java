package com.lazorko;

import java.util.*;

import static java.lang.Math.round;

public class App {  //the head class
    public static  void main(String[] args) {
        System.out.println("Enter a size of range: ");
        int num1 = getNumberFromConsole();
        int num2 = getNumberFromConsole();

        System.out.println("Range is from " + num1 + " to " + num2 + ";");
        if (validateRange(num1, num2)) {
            printOddNumbersFromInterval(num1, num2);
        }

        System.out.println("Enter size of set for Fibonacci range:");
        int fibonacciSize = getNumberFromConsole();
        if (validateFibonacciSize(fibonacciSize)) {
            printFibonacciInfo(fibonacciSize);
        }

    }

    private static final void printOddNumbersFromInterval(int num1, int num2) {
        Range range = getOddEvenArraysFromRange(num1, num2);
        System.out.println("Odd values: ");
        range.getOdd().forEach(System.out::println);

        range.getEven().sort(Collections.reverseOrder());
        System.out.println("Even values: ");
        range.getEven().forEach(System.out::println);

        System.out.print("Sum of odd values: ");
        int sumOddValues = range.getOdd().stream().reduce(0, Integer::sum);
        System.out.print("Sum of even values: ");
        int sumEvenValues = range.getEven().stream().reduce(0, Integer::sum);
        System.out.println(sumOddValues);
        System.out.println(sumEvenValues);
    }

    private static final Range getOddEvenArraysFromRange(int num1, int num2) {
        Range range = new App().new Range();
        for (int i = num1; i <= num2; i++) {
            if (i % 2 != 0) {
                range.getOdd().add(i);
            } else {
                range.getEven().add(i);
            }
        }
        return range;
    }

    private static Range getOddEvenArraysFromList(List<Integer> inputList) {
        Range range = new App().new Range();
        for (int i = 0; i < inputList.size(); i++) {
            int number = inputList.get(i);
            if (number % 2 != 0) {
                range.getOdd().add(number);
            } else {
                range.getEven().add(number);
            }
        }
        return range;
    }

    private static boolean validateRange(int num1, int num2) {
        return validateInterval(num1)
                && validateInterval(num2)
                && num1 <= num2;
    }

    private static boolean validateInterval(int number) {
        return number >= 1 && number <= 100;
    }

    private static boolean validateFibonacciSize(int number) {
        return number >= 1;
    }

    private static int getNumberFromConsole() {
        Scanner scanner = new Scanner(System.in);
        return scanner.nextInt();
    }

    private static final void printFibonacciInfo(int fibonacciSize) {
        System.out.println("Fibonacci: ");
        List<Integer> fibonacciList = getFibonacciList(fibonacciSize);
        fibonacciList.forEach(System.out::println);
        System.out.println("---------------------");

        Range fibonacciRange = getOddEvenArraysFromList(fibonacciList);
        Optional<Integer> f1 = fibonacciRange.getOdd().stream().
                max(Comparator.naturalOrder());
        Optional<Integer> f2 = fibonacciRange.getEven().stream().
                max(Comparator.naturalOrder());
        System.out.println("F1: " + f1.get() + "   F2: " + f2.get());

        double numberOfOddValues = fibonacciRange.getOdd().size();
        double oddPart = round(numberOfOddValues /
                (double) fibonacciSize * 100);
        System.out.println("Part of Odd values: " + oddPart + "%");
        System.out.println("Part of Even values: " + (100 - oddPart) + "%");
    }

    public static List<Integer> getFibonacciList(int N) {
        List<Integer> result = new ArrayList<>();
        if (N == 0) {
            return result;
        } else if (N == 1) {
            result.add(1);
        } else {
            int n1 = 0;
            int n2 = 1;
            while (result.size() < N) {
                result.add(n1);
                int sum = n1 + n2;
                n1 = n2;
                n2 = sum;
            }
        }
        return result;
    }

    public class Range {
        private List<Integer> odd;
        private List<Integer> even;

        public Range() {
            odd = new ArrayList<>();
            even = new ArrayList<>();
        }

        public Range(List<Integer> odd, List<Integer> even) {
            this.odd = odd;
            this.even = even;
        }

        public List<Integer> getOdd() {
            return odd;
        }

        public void setOdd(List<Integer> odd) {
            this.odd = odd;
        }

        public List<Integer> getEven() {
            return even;
        }

        public void setEven(List<Integer> even) {
            this.even = even;
        }

    }
}

